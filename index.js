const rp = require('request-promise');
const uuidv4 = require('uuid/v4');
const fetch = require('node-fetch');
const readlineSync = require('readline-sync');
const fs = require('fs');
const moment = require('moment');
const delay = require('delay');

console.log('');
console.log('');
const phoneNumber = readlineSync.question('Masukan No Hp : ');
console.log(`
    ======== MENU CODASHOP (megaxus-mi-cash-voucher) X GOPAY ========

    1. 10000 Mi-Cash
    2. 20000 Mi-Cash

`);


function getString(start, end, all) {
	const regex = new RegExp(`${start}(.*?)${end}`);
	const str = all
	const result = regex.exec(str);
	return result;
}

const genUniqueId = length =>
    new Promise((resolve, reject) => {
        var text = "";
        var possible =
            "abcdefghijklmnopqrstuvwxyz1234567890";

        for (var i = 0; i < length; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        resolve(text);
    });



const functionGojekSendOtp = (uuid, uniqid) => new Promise((resolve, reject) => {
    const url = 'https://api.gojekapi.com/v4/customers/login_with_phone'

    
    var options = {
        method: 'POST',
        uri: url,
        body: JSON.stringify({ "phone": `+62${phoneNumber}` }),
        headers: {
            'X-AppVersion': '3.30.2',
            'X-UniqueId': uniqid,
            'X-Platform': 'Android',
            'X-AppId': 'com.gojek.app',
            'Accept': 'application/json',
            'X-Session-ID': uuid,
            // 'D1': 'F1:00:6E:F2:2D:0D:F4:C5:67:6C:4B:D0:BC:22:DB:82:4A:44:90:53:E5:E1:22:14:D7:CD:A3:A9:37:BB:4E:B2',
            'X-PhoneModel': 'Android,Custom Phone - 6.0.0 - API 23 - 768x1280',
            'X-PushTokenType': 'FCM',
            'X-DeviceOS': 'Android,6.0',
            Authorization: 'Bearer',
            'Accept-Language': 'en-ID',
            'X-User-Locale': 'en_ID',
            'Content-Type': 'application/json; charset=UTF-8',
            'User-Agent': 'okhttp/3.12.1'
        },
    };

    rp(options)
    .then(function (body) {
        resolve(body)
    })
    .catch(function (err) {
        reject(err)
    });

  
});


const functionGojekVerify = (otpToken, otpLogin, uuid, uniqid) => new Promise((resolve, reject) => {
    const url = 'https://api.gojekapi.com/v4/customers/login/verify'
    
    var options = {
        method: 'POST',
        uri: url,
        body: JSON.stringify({
            "client_name": "gojek:cons:android",
            "client_secret": '83415d06-ec4e-11e6-a41b-6c40088ab51e',
            "data": {
                "otp": otpLogin,
                "otp_token": otpToken
            },
            "grant_type": "otp",
            "scopes": "gojek:customer:transaction gojek:customer:readonly"
        }),
        headers: {
            'X-AppVersion': '3.30.2',
            'X-UniqueId': uniqid,
            'X-Platform': 'Android',
            'X-AppId': 'com.gojek.app',
            'Accept': 'application/json',
            'X-Session-ID': uuid,
            // 'D1': 'F1:00:6E:F2:2D:0D:F4:C5:67:6C:4B:D0:BC:22:DB:82:4A:44:90:53:E5:E1:22:14:D7:CD:A3:A9:37:BB:4E:B2',
            'X-PhoneModel': 'Android,Custom Phone - 6.0.0 - API 23 - 768x1280',
            'X-PushTokenType': 'FCM',
            'X-DeviceOS': 'Android,6.0',
            Authorization: 'Bearer',
            'Accept-Language': 'en-ID',
            'X-User-Locale': 'en_ID',
            'Content-Type': 'application/json; charset=UTF-8',
            'User-Agent': 'okhttp/3.12.1'
        },
    };

    rp(options)
    .then(function (body) {
        resolve(body)
    })
    .catch(function (err) {
        reject(err)
    });
});




const getTrxId = (date, email, menu) => new Promise((resolve, reject) => {
    var options = {
        method: 'POST',
        uri: 'https://www.codashop.com/id/initPayment.action',
        formData: {
            'voucherPricePoint.id': parseInt(menu) === 1 ? 4136 :  4137 ,
            'voucherPricePoint.price': parseInt(menu) === 1 ? 11000 :  22000 ,
            'voucherPricePoint.variablePrice': 0,
            email: email,
            n: date,
            userVariablePrice: 0,
            msisdn: '',
           'order.data.profile': 'eyJuYW1lIjoiIiwiaWRfbm8iOiIifQ==',
            voucherTypeName: 'Inventary'
        },
        headers: {
            'accept': 'text/plain, */*; q=0.01',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'cookie':`shop-lang=in_ID; APP-NAME=CODASHOP; codashop_email=${email}; codashop_phone=`,
            'origin':'https://www.codashop.com',
            referer:'https://www.codashop.com/id/megaxus-mi-cash-voucher',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
        }
    };

    rp(options)
    .then(function (body) {
        resolve(body)
    })
    .catch(function (err) {
        reject(err)
    });
});

const getBarcodeCookie = (txnId, date) => new Promise((resolve, reject) => {
    fetch(`https://airtime.codapayments.com/airtime/begin?host_url=https%3A%2F%2Fwww.codashop.com%2Fid%2Fmegaxus-mi-cash-voucher&txn_id=${txnId}&client_type=1&client_time=${date}&_ga=2.114921735.1211339643.1566894885-1733291596.1566894885`, {
        method:'GET'
    })
    .then(res => {
        setCookie = res.headers.raw()['set-cookie'];
        fetch(`https://airtime.codapayments.com/airtime/epc-checkout?host_url=https://www.codashop.com/id/megaxus-mi-cash-voucher&txn_id=${txnId}&client_type=1&client_time=${date}`, {
            method:'GET',
            headers: {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
                Connection: 'keep-alive',
                'Cookie': `${setCookie.join().split(';')[2].split(',')[1]}; amfID=; language=in_ID; _gcl_au=1.1.502873712.1566895063; _fbp=fb.1.1566895062813.927544333; _ga=GA1.3.1414867406.1566895063; _gid=GA1.3.377020725.1566895063; _ga=GA1.2.1733291596.1566894885; _gid=GA1.2.1211339643.1566894885; session_pageview=1`,
                Host: 'airtime.codapayments.com',
                Referer: `https://airtime.codapayments.com/airtime/epc-checkout?host_url=https://www.codashop.com/id/megaxus-mi-cash-voucher&txn_id=${txnId}&client_type=1&client_time=${date}`,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
            }
        })
        .then(res => res.text())
        .then(result => resolve(result))
        .catch(err => reject(err))
    })
    .catch(err => reject(err))
    
})

const getBarcode = (txnId, date) => new Promise((resolve, reject) => {
    var options = {
        method: 'GET',
        uri: `https://airtime.codapayments.com/airtime/epc-msisdn?TxnId=${txnId}&browser_type=`,
        headers: {
            'cookie':'land_url="https://www.codashop.com/waiting?TxnId='+txnId+'&OrderId='+txnId+'";',
            'Host': 'airtime.codapayments.com',
            'Referer': `https://airtime.codapayments.com/airtime/begin?host_url=https://www.codashop.com/id/megaxus-mi-cash-voucher&txn_id=${txnId}&client_type=1&client_time=${date}`,
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
        }
    };

    rp(options)
    .then(function (body) {
        console.log(body)
        resolve(body)
    })
    .catch(function (err) {
        reject(err)
    });
});

const getRefereanceId = (qrcode) => new Promise((resolve, reject) => {
    console.log(qrcode)
    var options = {
        method: 'GET',
        uri: `https://zxing.org/w/decode?u=https://api.veritrans.co.id/v2/gopay/${qrcode}/qr-code`,
    };

    rp(options)
    .then(function (body) {
        resolve(body)
    })
    .catch(function (err) {
        reject(err)
    });
});

const payingCoda = (accessToken, otpLogin, uuid, uniqid, refid, promotion_id) => new Promise((resolve, reject) => {
    const url = 'https://api.gojekapi.com/v2/authorize_payment_request'
    
    var options = {
        method: 'POST',
        uri: url,
        body: JSON.stringify({"promotion_ids":[promotion_id],"reference_id":`${refid}`,"token":"eyJ0eXBlIjoiR09QQVlfV0FMTEVUIiwiaWQiOiIifQ=="}),
        headers: {
            'pin': otpLogin,
            'X-AppVersion': '3.30.2',
            'X-UniqueId': uniqid,
            'X-Platform': 'Android',
            'X-AppId': 'com.gojek.app',
            'Accept': 'application/json',
            'X-Session-ID': uuid,
            // 'D1': 'F1:00:6E:F2:2D:0D:F4:C5:67:6C:4B:D0:BC:22:DB:82:4A:44:90:53:E5:E1:22:14:D7:CD:A3:A9:37:BB:4E:B2',
            'X-PhoneModel': 'Android,Custom Phone - 6.0.0 - API 23 - 768x1280',
            'X-PushTokenType': 'FCM',
            'X-DeviceOS': 'Android,6.0',
            Authorization: `Bearer ${accessToken}`,
            'Accept-Language': 'en-ID',
            'X-User-Locale': 'en_ID',
            'Content-Type': 'application/json; charset=UTF-8',
            'User-Agent': 'okhttp/3.12.1'
        },
    };

    rp(options)
    .then(function (body) {
        resolve(body)
    })
    .catch(function (err) {
        reject({
            statusCode: err.statusCode,
            data: err.response.body
        })
    });
});

const potentionCashback = (accessToken, uuid, uniqid, refid) => new Promise((resolve, reject) => {
    const url = `https://api.gojekapi.com/v3/payment_request?reference_id=${refid}`
    
    var options = {
        method: 'GET',
        uri: url,
        headers: {
            'X-AppVersion': '3.30.2',
            'X-UniqueId': uniqid,
            'X-Platform': 'Android',
            'X-AppId': 'com.gojek.app',
            'Accept': 'application/json',
            'X-Session-ID': uuid,
            // 'D1': 'F1:00:6E:F2:2D:0D:F4:C5:67:6C:4B:D0:BC:22:DB:82:4A:44:90:53:E5:E1:22:14:D7:CD:A3:A9:37:BB:4E:B2',
            'X-PhoneModel': 'Android,Custom Phone - 6.0.0 - API 23 - 768x1280',
            'X-PushTokenType': 'FCM',
            'X-DeviceOS': 'Android,6.0',
            Authorization: `Bearer ${accessToken}`,
            'Accept-Language': 'en-ID',
            'X-User-Locale': 'en_ID',
            'Content-Type': 'application/json; charset=UTF-8',
            'User-Agent': 'okhttp/3.12.1'
        },
    };

    rp(options)
    .then(function (body) {
        resolve(JSON.parse(body))
    })
    .catch(function (err) {
        reject({
            statusCode: err.statusCode,
            data: err.response.body
        })
    });
});


(async () => {   
    const menu = await readlineSync.question('Pilih menu :  ');
    console.log('');
    if (parseInt(menu) === 1 || parseInt(menu) === 2) {
        const uuid = uuidv4();
        const uniqueid = await genUniqueId(16);
        console.log('');
        console.log(`[${moment().format("HH:mm:ss")}] Checking Session....`);
        await delay(3000)
        fs.readFile(`${phoneNumber}.session.txt`, 'utf8', async (err, data) => {
            if (err) {
                console.log(`[${moment().format("HH:mm:ss")}] Oops, No session found for number ${phoneNumber}`);
                console.log(`[${moment().format("HH:mm:ss")}] Creating New Session`);       
                console.log(`[${moment().format("HH:mm:ss")}] Send OTP To ${phoneNumber}`);
                const sendOtp = await functionGojekSendOtp(uuid, uniqueid);
                if (JSON.parse(sendOtp).success === false) {
                    console.log('');
                    console.log(sendOtp);
                    console.log('');
                }
                const login_token = JSON.parse(sendOtp).data.login_token;
                const otpLogin = await readlineSync.question('Masukan Otp : ');  
                const pin = await readlineSync.question('Masukan pin : ');
                const getAccessToken = await functionGojekVerify(login_token, otpLogin, uuid, uniqueid);
                const access_token = JSON.parse(getAccessToken).data.access_token
                await fs.appendFileSync(`${phoneNumber}.session.txt`, `${pin}:${access_token}`);
                console.log(`New session saved `);
                console.log('');               
                const email = await readlineSync.question('Masukan email untuk menerima code : ');
                console.log('');
                const date = moment().valueOf();
                const getTrx = await getTrxId(date, email, menu);
                const txnId = await JSON.parse(getTrx).txnId;
                console.log(`[${moment().format("HH:mm:ss")}] txnId : ${txnId}`);
                await delay(5000)
                const barcodeCookie = await getBarcodeCookie(txnId, date);
                const barcode = await getString("'paymentCode': '", "'", barcodeCookie);
                const getReferId = await getRefereanceId(barcode[1]);
                const getRefId = await getString("<tr><td>Raw text</td><td><pre>", "</pre></td></tr>", getReferId);
                const trefId = JSON.parse(getRefId[1]).data.tref;
                console.log(`[${moment().format("HH:mm:ss")}] referId : ${trefId}`);
                console.log(`[${moment().format("HH:mm:ss")}] Try To Pay with trefID : ${trefId}`);
                const getPotentionCashback = await potentionCashback(access_token, uuid, uniqueid, trefId);
                if (getPotentionCashback.data.promotions.length === 0) {
                    console.log(`[${moment().format("HH:mm:ss")}] Oops sudah tidak ada cashback :)`);
                }else{
                    console.log(`[${moment().format("HH:mm:ss")}] Anda akan mendapatkan ${getPotentionCashback.data.promotions[0].title} : ${getPotentionCashback.data.promotions[0].promotion_amount}`);
                    // console.log(getPotentionCashback)
                    const pay = await payingCoda(access_token, pin, uuid, uniqueid, trefId, getPotentionCashback.data.recommended_promotion_id);
                    console.log(JSON.parse(pay));
                    console.log('');
                }
            }else{
                console.log(`[${moment().format("HH:mm:ss")}] Session Found for number ${phoneNumber}`);
                console.log('');
                const email = await readlineSync.question('Masukan email untuk menerima code : ');
                console.log('');
                const pins = data.split(':')[0];
                const access_token = data.split(':')[1];
                const date = moment().valueOf();
                const getTrx = await getTrxId(date, email, menu);
                const txnId = await JSON.parse(getTrx).txnId;  
                console.log(`[${moment().format("HH:mm:ss")}] txnId : ${txnId}`);
                await delay(5000)
                const barcodeCookie = await getBarcodeCookie(txnId, date);
                const barcode = await getString("'paymentCode': '", "'", barcodeCookie);
                const getReferId = await getRefereanceId(barcode[1]);
                const getRefId = await getString("<tr><td>Raw text</td><td><pre>", "</pre></td></tr>", getReferId);
                const trefId = JSON.parse(getRefId[1]).data.tref;
                console.log(`[${moment().format("HH:mm:ss")}] referId : ${trefId}`);
                console.log(`[${moment().format("HH:mm:ss")}] Try To Pay with trefID : ${trefId}`);
                const getPotentionCashback = await potentionCashback(access_token, uuid, uniqueid, trefId);
                if (getPotentionCashback.data.promotions.length === 0) {
                    console.log(`[${moment().format("HH:mm:ss")}] Oops sudah tidak ada cashback :)`);
                }else{
                    console.log(`[${moment().format("HH:mm:ss")}] Anda akan mendapatkan ${getPotentionCashback.data.promotions[0].title} : ${getPotentionCashback.data.promotions[0].promotion_amount}`);
                    const pay = await payingCoda(access_token, pins, uuid, uniqueid, trefId, getPotentionCashback.data.recommended_promotion_id);
                    console.log(JSON.parse(pay));
                    console.log('');
                }
            }          
        });
        
    }else{
        console.log('Woahh, menu notfound :(');
    }
    
})();